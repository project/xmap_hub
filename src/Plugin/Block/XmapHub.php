<?php

namespace Drupal\xmap_hub\Plugin\Block;

use Drupal\ukscplugin\Helper;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "xmap_hub",
 *   admin_label = @Translation("XMAP Hub"),
 * )
 */
class XmapHub extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build()
  {


    $args = array();

    $xmap_url = $this->configuration['xmap_url'];
    $font_family = $this->configuration['font_family'];
    $primary_color = $this->configuration['primary_color'];
    $secondary_color = $this->configuration['secondary_color'];
    $bg_quick_links_color = $this->configuration['bg_quick_links_color'];
    $head_heading_color = $this->configuration['head_heading_color'];
    $head_sub_heading_color = $this->configuration['head_sub_heading_color'];
    $body_background = $this->configuration['body_background'];

    $width = $this->configuration['width'];


    array_push( $args, 'i=1');
    array_push( $args, 'c=1');
    if( $primary_color ) array_push( $args, 'primary_color=' . urlencode($primary_color) );
    if( $font_family ) array_push( $args, 'font_family=' . urlencode($font_family) );
    if( $secondary_color ) array_push( $args, 'secondary_color=' . urlencode($secondary_color) );
    if( $bg_quick_links_color ) array_push( $args, 'bg_quick_links_color=' . urlencode($bg_quick_links_color ));
    if( $head_heading_color ) array_push( $args, 'head_heading_color=' . urlencode($head_heading_color) );
    if( $head_sub_heading_color ) array_push( $args, 'head_sub_heading_color=' . urlencode($head_sub_heading_color) );
    if( $body_background ) array_push( $args, 'body_background=' . urlencode($body_background) );
    if( $width ) array_push( $args, 'width=' . urlencode($width) );

    $queryParams = implode ("&", $args);
    $url = $xmap_url . '?' . $queryParams;

    return [
      '#theme' => 'xmap_hub_content',
      '#url' => $url
    ];
  }

  public function defaultConfiguration()
  {
    return [
      'primary_color' => null,
      'secondary_color' => null,
      'bg_quick_links_color' => null,
      'head_heading_color' => null,
      'head_sub_heading_color' => null,
      'body_background' => null,
      'font_family' => null,
      'xmap_url' => 'https://demo.hub.xmap.cloud/',
      'width' => '1164',
    ];
  }

  public function blockForm($form, FormStateInterface $form_state)
  {

    $form['xmap_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('XMAP Url'),
      '#description' => $this->t('The xmap url'),
      '#default_value' => $this->configuration['xmap_url'],
    ];

    $form['font_family'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Font'),
      '#description' => $this->t('Google font eg. Roboto'),
      '#default_value' => $this->configuration['font_family'],
    ];


    $form['primary_color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Primary color'),
      '#description' => $this->t('eg. #676767'),
      '#default_value' => $this->configuration['primary_color'],
    ];

    $form['secondary_color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secondary color'),
      '#description' => $this->t('eg. #676767'),
      '#default_value' => $this->configuration['secondary_color'],
    ];


    $form['bg_quick_links_color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Quick Links background color'),
      '#description' => $this->t('eg. #F3F2F1'),
      '#default_value' => $this->configuration['bg_quick_links_color'],
    ];

    $form['head_heading_color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Heanding font color'),
      '#description' => $this->t('eg. #ffffff'),
      '#default_value' => $this->configuration['head_heading_color'],
    ];

    $form['head_sub_heading_color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sub heanding color'),
      '#description' => $this->t('eg. #ffffff'),
      '#default_value' => $this->configuration['head_sub_heading_color'],
    ];

    $form['body_background'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Page background color'),
      '#description' => $this->t('eg. #ffffff'),
      '#default_value' => $this->configuration['body_background'],
    ];





    $form['width'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Max Width in pixels'),
      '#description' => $this->t('eg. 1164'),
      '#default_value' => $this->configuration['width'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();
    $this->configuration['xmap_url'] = $values['xmap_url'];
    $this->configuration['font_family'] = $values['font_family'];
    $this->configuration['primary_color'] = $values['primary_color'];
    $this->configuration['secondary_color'] = $values['secondary_color'];
    $this->configuration['bg_quick_links_color'] = $values['bg_quick_links_color'];
    $this->configuration['head_heading_color'] = $values['head_heading_color'];
    $this->configuration['head_sub_heading_color'] = $values['head_sub_heading_color'];
    $this->configuration['body_background'] = $values['body_background'];
    $this->configuration['width'] = $values['width'];
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state)
  {

    if ($form_state->getValue('xmap_url') === 'empty') {
      $form_state->setErrorByName(
        'xmap_url',
        $this->t('Not a valid url')
      );
    }

    if (!is_numeric($form_state->getValue('width'))) {
      $form_state->setErrorByName(
        'width',
        $this->t('Must be a number')
      );
    }

  }

}
